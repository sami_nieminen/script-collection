#Requires -Modules activedirectory

param(
    [Parameter(Mandatory=$true)]
    [ValidateRange(10,365)]
    [Int]$DaysInactive,
    [switch]$QueryOnly,
    [switch]$DisableAndMove,
    [switch]$SendMail
)

# Gets time stamps for all computers in the domain that have NOT logged in since after specified date 
# Sami Nieminen @ Inmics-Nebula Oy 17.12.2021
# V. 2.1 

# Define global variables for script
$global:domain = "domain.com"  
$global:time = (Get-Date).Adddays(-($DaysInactive)) 
$global:logfile = "$PSScriptRoot\logfile.txt"
$global:Date = Get-Date -Format g
$global:Description = "Disabled at $Date by script"
$global:DisabledOU = "OU=Computers, OU=Disabled, OU=Computers, DC=domain, DC=com"
$global:DisableCSV = "$PSScriptRoot\computers_to_be_disabled.csv"

# Define logging timestamp function
function Get-TimeStamp {
    return "[{0:dd/MM/yy} {0:HH:mm:ss}]" -f (Get-Date)
}

function SetUpFiles {
    #If the logfile does not exist, create it.
    if (-not(Test-Path -Path $logfile -PathType Leaf)) {
        try {
            $null = New-Item -ItemType File -Path $logfile -Force -ErrorAction Stop
            Write-Host "The file [$logfile] has been created."
        }
        catch {
            throw $_.Exception.Message
        }
    }
    else {
    
    }

    #If the disable csv file does not exist, create it.
    if (-not(Test-Path -Path $DisableCSV -PathType Leaf)) {
        try {
            $null = New-Item -ItemType File -Path $DisableCSV -Force -ErrorAction Stop
            Write-Host "The file [$DisableCSV] has been created."
        }
        catch {
            throw $_.Exception.Message
        }
    }
    else {
    
    }

    # Clear contents of the temp CSV we are using
    Clear-Content $DisableCSV

    # Clear the variable we use for disabling computers
    $disablecomputers = ""

    # Import AD module
    import-module activedirectory 
}

# Get all AD computers with lastLogonTimestamp less than our time in all DC's
function GetInactiveComputers {
    SetUpFiles
    # Define variables for command
    $SearchOU = "OU=Computers, DC=domain, DC=com"
    Get-ADComputer -Filter {LastLogonTimeStamp -lt $time} -Properties LastLogonTimeStamp -SearchBase $SearchOU | select-object -expandproperty name | Out-File $DisableCSV -Append

    # Add computers to variable for the disable loop
    $global:disablecomputers = Get-Content -Path $DisableCSV

}

# Main loop for disabling + moving computers
function DisableComputers {
    Foreach ($Computer in $disablecomputers){
        Try {
            $ExistingDescription = (Get-ADComputer -Identity $Computer -Properties Description).Description
            $NewDescription = $ExistingDescription + " "+ $Description
            Set-ADComputer -Identity $Computer -Description $NewDescription -Enabled $false -ErrorAction stop
            Write-Output "$(Get-Timestamp) Setting computer $Computer description" | Out-File $logfile -append
            $DistinguishedName = (Get-ADComputer -Identity $Computer).DistinguishedName
            Move-ADObject -Identity $DistinguishedName -TargetPath $DisabledOU -ErrorAction stop
            Write-Output "$(Get-Timestamp) Moving computer $Computer to Disabled OU" | Out-File $logfile -append
            }
        Catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]{
            Write-Output "$(Get-Timestamp) $Computer was not found" | Out-File $logfile -append
        }
        Catch [Microsoft.ActiveDirectory.Management.ADException]{
            Write-Output "$(Get-Timestamp) No permission to $Computer" | Out-File $logfile -append
        }
    }
}

# Send mail to inform what has been disabled
function SendInfoMail {
    # Define variables for command
    $Sender = "no-reply@domain.com"
    $Receiver = "it@domain.com"
    $Subject = "Disabloidut konetilit $Date"
    $CustomMessage = "<b>Seuraavat konetilit on disabloitu $Domain domainista </b><br><br>$disablecomputers <br><br> Mukana my&ouml;s CSV tiedosto disabloiduista koneista"
    $SMTPRelay = "smtp.domain.com"

    # Use powershell to send mail
    Send-MailMessage -From $Sender -Subject $Subject -To $Receiver -Body $CustomMessage -BodyAsHtml -SmtpServer $SMTPRelay -Attachments $DisableCSV
}


if ($QueryOnly) {
    GetInactiveComputers
    Write-Output "$(Get-Timestamp) Running in QueryOnly, check $DisableCSV for computers" | Out-File $logfile -append
} else {
    GetInactiveComputers
    DisableComputers
    if ($SendMail) {
        SendInfoMail
    } else {}
}